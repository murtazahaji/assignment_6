/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dhair
 */
public class PersonDirectory {
        private ArrayList<Person> personDirectory;
    
    public PersonDirectory(){
    personDirectory = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonArrayList() {
        return personDirectory;
    }

    public void setPersonArrayList(ArrayList<Person> personArrayList) {
        this.personDirectory = personArrayList;
    }
    
    public Person addPerson()
    {
        Person p = new Person();
        personDirectory.add(p);
        return p;
    
    }
    
}


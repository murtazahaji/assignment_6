/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dhair
 */
public class Business {
    
    private SupplierDirectory supplierDirectory;
    private CustomerDirectory customerDirectory;
    private EmployeeDirectory employeeDirectory;
    private MarketDirectory marketDirectory;
    private MarketOfferDirectory marketOfferDirectory;
    private String name;
    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;
    
    public Business(){
    supplierDirectory = new SupplierDirectory();
    customerDirectory = new CustomerDirectory();
    employeeDirectory = new EmployeeDirectory();
    marketDirectory = new MarketDirectory();
    marketOfferDirectory = new MarketOfferDirectory();
    personDirectory = new PersonDirectory();
    userAccountDirectory = new UserAccountDirectory();
    
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public MarketDirectory getMarketDirectory() {
        return marketDirectory;
    }

    public void setMarketDirectory(MarketDirectory marketDirectory) {
        this.marketDirectory = marketDirectory;
    }


    public MarketOfferDirectory getMarketOfferDirectory() {
        return marketOfferDirectory;
    }

    public void setMarketOfferDirectory(MarketOfferDirectory marketOfferDirectory) {
        this.marketOfferDirectory = marketOfferDirectory;
    }

  
    
    
    
    
}

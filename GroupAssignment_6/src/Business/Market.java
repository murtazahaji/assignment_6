/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author murta
 */
public class Market {
    
    private String marketType;
    private String marketOffer;

    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }

    public String getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(String marketOffer) {
        this.marketOffer = marketOffer;
    }
    
    
    
}

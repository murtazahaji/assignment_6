/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dhair
 */
public class ProductCatalog {
    
    private ArrayList<Product> productList;
    
    public ProductCatalog()
    {
        productList = new ArrayList<Product>();
    }
   
    
    public ArrayList<Product> getProductArray() {
        return productList;
    }

    public void setProductArray(ArrayList<Product> productArray) {
        this.productList = productArray;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author murta
 */
public class Order {
    
    private String status;
    private Date issueDate;
    private Date completionDate;
    private Date shippingDate;
    private SalesPerson salesPerson;    
    private ArrayList<OrderItem> orderItemList;

    public Order(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }
    
    
    
}
